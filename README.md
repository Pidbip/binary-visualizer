# BinVis

BinVis enables you to analyze code and data visually

  - Written in C++
  - Qt is used for the GUI
  - Can handle massive amounts of data


## Documentation
[Webpage](http://trippler.no/wpcms/?page_id=20)

[Changelog](http://trippler.no/wpcms/?page_id=64)

## Releases
[Unstable windows 7/8 release](https://bitbucket.org/trippler/binary-visualizer/downloads/binvis%20release_windows_0.1.zip)

## Building from source

###Linux
```sh
$ git clone  https://bitbucket.org/trippler/binary-visualizer
$ cd binary-visualizer/binvis-qt
$ qmake binvis-qt.pro
$ make
```
###OS X

```sh
$ git clone  https://bitbucket.org/trippler/binary-visualizer
$ cd binary-visualizer/binvis-qt
```

###Windows
```sh
$ git clone  https://bitbucket.org/trippler/binary-visualizer
$ cd binary-visualizer\binvis-qt
```
### Development

Want to contribute? Great!
We have a [Trello board](https://trello.com/b/BD1Ow8Kc/binary-visualizer)

License
----

GPL v2