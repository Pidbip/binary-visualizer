/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "task_manager.h"
#include "task_consumer.h"
#include "task.h"
#include "tasks/task_idle.h"
#include "tasks/task_maintenance_cleanup.h"
#include "tasks/task_execute_every.h"

Task_manager::Task_manager(int threads)
{
	running = true;
	idle_task = new Task_idle("Idle");
	for(int n = 0; n < threads; ++n){
		Task_consumer* tc = new Task_consumer(this);
		consumers.push_back(tc);
		consumer_threads.push_back(new std::thread(task_consumer_thread, *tc));
	}
	maintenance = new Task_maintenance_cleanup();
	maintenance_timer = new Task_execute_every(std::chrono::seconds(1), maintenance);
	add_task(maintenance_timer);
	executed_tasks = 0;
}

Task_manager::~Task_manager()
{
	for(std::thread* t : consumer_threads){
		if(t->joinable())
			t->join();
		delete t;
	}
	for(Task_consumer* c : consumers){
		delete c;
	}
	delete idle_task;
	delete maintenance;
	delete maintenance_timer;
	perform_maintenance();
}

void Task_manager::add_task(Task *t, bool high_priority)
{
	lock_tasks();
	if(high_priority)
		this->tasks.push_front(t);
	else if (this->running)
		this->tasks.push_back(t);
	unlock_tasks();
	((Task_idle*)idle_task)->wake();
}

bool Task_manager::more_tasks_available()
{
	return tasks.size() > 0;
}

size_t Task_manager::tasks_available()
{
	return tasks.size();
}

Task* Task_manager::get_task()
{
	Task* task = NULL;
	lock_tasks();
	for(Task* t : tasks){
		if(t->is_executable()){
			task = t;
			tasks.remove(t);
			break;
		}
	}
	unlock_tasks();
	++executed_tasks;
	if(task == NULL && this->running)
		return idle_task;

	return task;
}

bool Task_manager::is_running()
{
	return running;
}

void Task_manager::stop()
{
	running = false;
	((Task_idle*)idle_task)->wake();
	for(std::thread* t : consumer_threads){
		t->join();
	}
}

unsigned long Task_manager::get_executed_task_count()
{
	return executed_tasks;
}

void Task_manager::add_task_for_deletion(Task* t)
{
	lock_task_deletion();
	to_be_deleted.push_back(t);
	unlock_task_deletion();
}

void Task_manager::perform_maintenance()
{
	lock_task_deletion();
	Task* t;
	std::list<Task*>::iterator iter = to_be_deleted.begin();
	while(iter != to_be_deleted.end()){
		t = *iter;
		iter++;
		if (t->completed() && t->depending_completed()) {
			to_be_deleted.remove(t);
			delete t;
		}
	}
	unlock_task_deletion();
}

void Task_manager::lock_tasks()
{
	task_mutex.lock();
}

void Task_manager::unlock_tasks()
{
	task_mutex.unlock();
}

void Task_manager::lock_task_deletion()
{
	delete_mutex.lock();
}

void Task_manager::unlock_task_deletion()
{
	delete_mutex.unlock();
}
