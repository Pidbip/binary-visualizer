/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "thread"
#include "chrono"
#include "ratelimited_task_manager.h"

Ratelimited_task_manager::Ratelimited_task_manager(int max_tasks, int threads) : Task_manager(threads)
{
	this->max_tasks = max_tasks;
}

void Ratelimited_task_manager::add_task(Task* t, bool high_priority)
{
	while(true){
		lock_tasks();
		if(tasks.size() < max_tasks)
			break;
		unlock_tasks();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	unlock_tasks();
	Task_manager::add_task(t, high_priority);
}
