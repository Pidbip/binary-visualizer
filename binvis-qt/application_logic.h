/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef APPLICATION_LOGIC_H
#define APPLICATION_LOGIC_H
#include <QObject>
#include "displaydata.h"
#include "glwidget.h"
#include "parallel_indice_sorter.h"
#include "partfile_controller.h"
class parallel_indice_sorter_master;
class application_logic : QObject
{
    Q_OBJECT
public:
    application_logic();
    application_logic(QObject* parent);
    ~application_logic();
    void set_glwidget_window(GLWidget* glwidget);
    void set_file(const char* filename);

private:
    void init_displaydata();
    void optimize();
    GLWidget* glwidget;
    int optimization_level;
    parallel_indice_sorter_master parallel_sorter;
    unsigned int* index_ptr;
    int index_count;
    int indices_to_draw;
    bool reverse_sort;
    long int filesize;
    displaydata display_data;
    QTimer *rotateTimer;

public:
    partfile_controller file_controller;
    void indice_sortering_finished();
    unsigned int *get_index_list();
    size_t get_index_size();
    int get_sorting_level();
	int get_draw_count();
    void set_min_filter(int filter);
    void add_buffer(char *buffer, size_t count);
    void remove_buffer(char* buffer, size_t count);
    void set_offsets(long int start, long int stop);
    void redraw_frames();
    void set_bit_alignment(int alignment);
    void set_point_scaling(int scaling);
    void set_reverse_draw(bool state);
public slots:
    void set_rotation_xy(float x_angle, float y_angle);
    void set_rotation_x(float angle);
    void set_rotation_y(float angle);
    float get_rotation_x();
    float get_rotation_y();
    long get_file_size();
    void auto_rotate(bool arg);
signals:
    void rerun_optimize();
private slots:
    void run_optimize();
};

#endif // APPLICATION_LOGIC_H
