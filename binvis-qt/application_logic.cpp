/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <QObject>
#include "application_logic.h"
#include "error_messages.h"
#include "plotter.h"
#include "parallel_indice_sorter.h"

#include <QDateTime>

#define NUMBER_OF_THREADS_TO_USE_FOR_SORTING 8
#define UNSORTED 0
#define SORTING 1
#define SORTED 2
#define RESORT 3

application_logic::application_logic()
{
    init_displaydata();
    index_ptr = (unsigned int*)malloc(256*256*256*sizeof(unsigned int));
    if(!index_ptr){
        ERROR("Could not allocate memory for index");
        exit(1);
    }
    index_count = 256*256*256;
    for(unsigned int n = 0; n < (unsigned int)index_count; ++n)
        index_ptr[n] = n;
    QObject::connect(this, SIGNAL(rerun_optimize()), this, SLOT(run_optimize()), Qt::QueuedConnection);

    rotateTimer = nullptr;
}

application_logic::~application_logic()
{
    INFO("Freeing indices (run little ones)");
    free(index_ptr);

#ifdef _WIN32
    for(int n = 0; n < 256; ++n)
        DeleteCriticalSection(&(display_data.vertices.point_mutex[n]));
#endif
}

void application_logic::set_glwidget_window(GLWidget *glwidget)
{
    this->glwidget = glwidget;
    this->glwidget->set_displaydata(&display_data);
    this->glwidget->set_application_logic_ptr(this);
    this->glwidget->request_indice_reupload();
}

void application_logic::set_file(const char *filename)
{
    optimization_level = UNSORTED;
    indices_to_draw = 256*256*256;
    init_displaydata();
    /*filesize = plot_file((char*)filename, &display_data);
    file_controller.set_currently_active_buffer_offsets(0, filesize);
    file_controller.set_filename(filename);*/
    filesize = file_controller.set_initial_file(filename, &display_data);
    INFO("Filesize for %s is %ld bytes", filename, filesize);
    glwidget->request_vertex_reupload();
    optimize();
}

void application_logic::init_displaydata()
{
    INFO("Initializing displaydata");
    display_data.shaders.shader_program = 0;
    display_data.shaders.vertex_shader = 0;
    display_data.shaders.fragment_shader = 0;

    static bool allocated = false;
    if(!allocated){
        display_data.vertices.points = (struct point*)malloc(256*256*256*sizeof(struct point));
        allocated = true;
    }
    memset(display_data.vertices.points, 0, 256*256*256*sizeof(struct point));
    float number = 1.0f/256.0f;
    for(int n = 0; n < 256; ++n)
        for(int m = 0; m < 256; ++m)
            for(int o = 0; o < 256; ++o){
                display_data.vertices.points[n*256*256 + m*256 + o].x = number*(float)n - 0.5f;
                display_data.vertices.points[n*256*256 + m*256 + o].y = number*(float)m - 0.5f;
                display_data.vertices.points[n*256*256 + m*256 + o].z = number*(float)o - 0.5f;
                display_data.vertices.points[n*256*256 + m*256 + o].count = 0.0f;
            }

    static bool translations_initialized = false;
    if(!translations_initialized){
        display_data.translations.rotation_x = 0.0f;
        display_data.translations.rotation_y = 0.0f;
        display_data.translations.min_count = 1;
        display_data.translations.alpha = 0.5f;
        display_data.translations.steps = 10;
        display_data.translations.pos_scale = 1.0f;
        translations_initialized = true;
    }
    //Initialize mutexes for windows
#ifdef _WIN32
    for(int n = 0; n < 256; ++n)
        InitializeCriticalSection(&(display_data.vertices.point_mutex[n]));
#endif
}

void application_logic::optimize()
{
    if(optimization_level == SORTING || optimization_level == RESORT){
        optimization_level = RESORT;
        return;
    }
    this->optimization_level = SORTING;
    parallel_sorter.reversed = reverse_sort;
    parallel_sorter.set_displaydata(&display_data);
    parallel_sorter.set_thread_count(NUMBER_OF_THREADS_TO_USE_FOR_SORTING);
    parallel_sorter.set_indices(index_ptr, index_count);
    parallel_sorter.set_application_callback(this);
    parallel_sorter.start();
}

void application_logic::indice_sortering_finished()
{
    if(optimization_level == RESORT){
        optimization_level = UNSORTED;
        emit rerun_optimize();
        return;
    }
    glwidget->request_indice_reupload();
    this->set_min_filter(display_data.translations.min_count);
    this->optimization_level = SORTED;
    this->redraw_frames();
}

unsigned int* application_logic::get_index_list()
{
    return index_ptr;
}

size_t application_logic::get_index_size(){
    return 256*256*256*sizeof(unsigned int);
}

int application_logic::get_sorting_level()
{
    return optimization_level;
}

int application_logic::get_draw_count()
{
    return indices_to_draw;
}

void application_logic::set_min_filter(int filter)
{
    display_data.translations.min_count = (float)filter;
    indices_to_draw = find_mincount_index_number(&display_data, index_ptr, display_data.translations.min_count, reverse_sort);
    redraw_frames();
}

void application_logic::add_buffer(char *buffer, size_t count)
{
    add_buffer_to_displaydata(buffer, count, &display_data);
    glwidget->request_vertex_reupload();
    optimize();
}

void application_logic::remove_buffer(char *buffer, size_t count)
{
    remove_buffer_from_displaydata(buffer, count, &display_data);
    glwidget->request_vertex_reupload();
    optimize();
}

void application_logic::set_offsets(long int start, long int stop)
{
    INFO("Setting new offsets(%ld,%ld).", start, stop);
    if(start < 0 || stop <= 0 || stop <= start){
        ERROR("Offset checks failed.");
        return;
    }

    bool offsets_modified = file_controller.set_buffer_offsets(start, stop, &display_data);
    glwidget->request_vertex_reupload();
    if(offsets_modified)
        optimize();
}

void application_logic::redraw_frames()
{
    glwidget->request_frame_redraw();
}

void application_logic::set_bit_alignment(int alignment)
{
    this->file_controller.set_bit_alignment(alignment, &display_data);
    glwidget->request_vertex_reupload();
    optimize();
}

void application_logic::set_point_scaling(int scaling)
{
    this->file_controller.set_point_scaling(scaling, &display_data);
    glwidget->request_vertex_reupload();
    optimize();
}

void application_logic::set_reverse_draw(bool state)
{
    this->reverse_sort = state;
    glwidget->set_reverse_draw(state);
    optimize();
}

void application_logic::set_rotation_x(float angle)
{
    display_data.translations.rotation_x = angle;
    redraw_frames();
}

void application_logic::set_rotation_y(float angle)
{
    display_data.translations.rotation_y = angle;
    redraw_frames();
}

void application_logic::auto_rotate(bool arg) {

    if (rotateTimer) {
        if (rotateTimer->isActive()) {
            rotateTimer->stop();
            rotateTimer->deleteLater();
            rotateTimer = nullptr;
        }
    }

    else {
        rotateTimer = new QTimer(this);

        connect(rotateTimer, &QTimer::timeout, [=] (float x_angle=0.20, float y_angle=0.10) {
            set_rotation_xy(x_angle,y_angle);
        });

        rotateTimer->start(20);
    }
}

void application_logic::set_rotation_xy(float x_angle, float y_angle)
{
    display_data.translations.rotation_x -= x_angle;
    display_data.translations.rotation_y += y_angle;
    redraw_frames();
}

float application_logic::get_rotation_x() {
    return display_data.translations.rotation_x;
}

float application_logic::get_rotation_y() {
    return display_data.translations.rotation_y;
}

long int application_logic::get_file_size()
{
    return filesize+1;
}

void application_logic::run_optimize()
{
    optimize();
}
