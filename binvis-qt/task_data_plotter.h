/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "libtask/task.h"
#include "alignment_tables.h"
#include "plotter.h"
#include "displaydata.h"

#ifndef TASK_DATA_PLOTTER_H
#define TASK_DATA_PLOTTER_H

class Task_data_plotter : public Task
{
public:
    Task_data_plotter();
    ~Task_data_plotter();
    void set_properties(struct filedatabuffer* fdb, int x, int y, struct displaydata* display_data, float add_value);
    void execute(Task_manager* manager);
private:
    unsigned long long int data_length;
    unsigned char* data;
    struct filedatabuffer buffer;
    long long alignment_max_value;
    int alignment_scaling;
    struct displaydata* display_data;
    int x, y;
    float add_value;
};

#endif // TASK_DATA_PLOTTER_H
