/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef sl_constvar_h
#define sl_constvar_h
#include "bufferex.h"
#include <pthread.h>
#include <list>
#include <stdint.h>
using std::list;
//Type used to hold sizes of elements. This needs to be able to contain more then max size of an element or a buffer overflow occurs
typedef int64_t esize;
class cvarElement{
    public:
    //element id (like 0, 1, 2, 3...)
	uint32_t ID;
	//Length of name
	esize nameSize;
	//name
	char *name;
	//size, using int64_t to support >4G of data (is size_t 64-bit?)
	esize dataSize;
	bufferex data;
	char *getData();
	//type, some enum to be added later
	int32_t type;
	//Should this element be saved?
	bool save;
	//init
	cvarElement(uint32_t unID);
	//deinit
	~cvarElement();
	//Set name for element
    bool setName(const char *name, esize nameLength);
	//Set data for element
    bool setContent(const char *content, esize contentSize);
	//Add data to end of element
    bool appendData(const char *content, esize contentSize);
};
/*
	Saving asyncron*/
class cvarList{
	//Temporarily make all public
    public:
    pthread_mutex_t ownMutex;
	list < cvarElement *> elements;
	list < cvarElement *>::iterator Iter;
	//Used to store variables
	char *listName;
	//Status of last command sent to class; 0 = no error
	int status;
	int cID;
	cvarList();
    cvarList(const char *fileName);
	~cvarList();
	//Mutexes to lock and unlock access
	void lock();
	void unlock();
	//Search for a variable, if not found returns NULL
	//+Locks
	cvarElement *get(const char *navn);
	//This will add a variable unless it allready exists
    cvarElement *set(const char *navn, const char *content, esize contentSize);
	//This will also add a variable unless exists; but will not empty content if it does exist.
    cvarElement *append(const char *navn, const char *content, esize contentSize);
	//Save variables to file
	bool save();
	//Re-load file (Note this will delete all variables and reload the ones in the file)
	bool load();
	void empty();
};
//#include "constvar.cpp"
#endif
