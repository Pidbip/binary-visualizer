/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDropEvent>
#include <QUrl>
#include <QDebug>
#include <QMimeData>
#include <QShortcut>
#include <QLabel>
#include <QColor>
#include <QRgb>
#include <QClipboard>
#include "settings_window.h"
#include "constvar/constvar.h"
#include "application_logic.h"
#include "byte_plot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //Controller callbacks from the main window
private slots:
    void on_comboBox_activated(const QString &arg1);
    void on_combo_rotation_activated(const QString &arg1);
    void on_lineEdit_textChanged(const QString &arg1);
    void settings_clicked();
    void settings_accepted();
    void toggleFullScreen();
    void on_button_set_offsets_clicked();
    void file_open_clicked();
    void offset_handle();
    void window_scroller(bool direction);
    void bit_alignment_scroller(int direction);
    void bit_alignment_modified();
    void point_scaling_modified();
    void copy_file_info_to_clipboard();
    void read_file_info_from_clipboard();
    void byte_plot_clicked();
    void hide_menu_slot();
protected:
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);

private:
    Ui::MainWindow *ui;
    settings_window* settings;
    QLabel *file_path_label;
    QLabel *window_pos_label;
    QLabel *window_size_label;
    QLabel *zoom_level_label;
    QLabel *point_size_label;
    QLabel *transparency_label;
    QLabel *rotation_label;
    QLabel *steps_label;
    QLabel *bit_alignment_label;
    byte_plot* byte_plot_window;

    cvarList* constant_variables;
    bool return_to_maximized;
    application_logic visualization;
    void update_labels(long int window_size, long int window_start, long int window_stop);
    void open_file(QString filename);
    void create_status_bar();
    QPalette get_palette();
    QString current_file;
    long int filesize;
};


#endif // MAINWINDOW_H
