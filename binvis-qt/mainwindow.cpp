/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "error_messages.h"
#include <QFileDialog>
#include "constvar/constvar.h"
#include "qstatusbar.h"
#include "auxilary_functioons.h"
#include "qlabel.h"
#include "qrgb.h"
#include "qclipboard.h"
#include "qcolor.h"
#include "global_task_manager.h"
#define MAX(a,b) (a>b)?a:b
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    visualization.set_glwidget_window(ui->opengl_window);

    constant_variables = new cvarList("settings.cvar");
    constant_variables->load();

    if(constant_variables->get("palette_red") == NULL)
        constant_variables->set("palette_red", "240", strlen("240"));

    if(constant_variables->get("palette_green") == NULL)
        constant_variables->set("palette_green", "240", strlen("240"));

    if(constant_variables->get("palette_blue") == NULL)
        constant_variables->set("palette_blue", "240", strlen("240"));

    if (constant_variables->get("last_folder") == NULL) {
        constant_variables->set("last_folder", " ", strlen(" "));
    }

    if (constant_variables->get("orthographical_view") == NULL) {
        constant_variables->set("orthographical_view", "1", strlen("1"));
    }

    if (constant_variables->get("reverse_draw") == NULL) {
        constant_variables->set("reverse_draw", "0", strlen("0"));
    }


    //Shortcuts
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this, SLOT(close()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this, SLOT(file_open_clicked()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(settings_clicked()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_C), this, SLOT(copy_file_info_to_clipboard()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_V), this, SLOT(read_file_info_from_clipboard()));
    new QShortcut(QKeySequence(Qt::Key_F11), this, SLOT(toggleFullScreen()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_H), this, SLOT(hide_menu_slot()));

    QShortcut *window_scroll_up_shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_PageUp), this);
    QShortcut *window_scroll_down_shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_PageDown), this);
    QShortcut *bit_alignment_increase_shortcut = new QShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageUp), this);
    QShortcut *bit_alignment_decrease_shortcut = new QShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageDown), this);
    QShortcut *auto_rotate_toggle = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_R), this);

    //Lambdas!
    connect(auto_rotate_toggle, &QShortcut::activated, [=] (bool arg = true){
        visualization.auto_rotate(arg);
    });

    connect(ui->opengl_window, &GLWidget::point_size_modified_signal, [=] (int size) {
        point_size_label->setText(QString::number(size));
    });

    connect(ui->opengl_window, &GLWidget::transperency_modified_signal, [=] (float value) {
        transparency_label->setText(QString::number(value));
    });

    connect(ui->opengl_window, &GLWidget::steps_modified_signal, [=] (int steps) {
        steps_label->setText(QString::number(steps));
    });

    connect(ui->opengl_window, &GLWidget::rotated_signal, [=] (float x, float y) {
        rotation_label->setText(QString::number(x) + "," + QString::number(y));
    });

    connect(ui->opengl_window, &GLWidget::wheel_scrolled_signal, [=] (float scale) {
        zoom_level_label->setText(QString::number(scale));
    });

    connect(window_scroll_up_shortcut, &QShortcut::activated, [=](bool arg=true) {
        window_scroller(arg);
    } );
    connect(window_scroll_down_shortcut, &QShortcut::activated, [=](bool arg=false) {
        window_scroller(arg);
    } );

    connect(bit_alignment_increase_shortcut, &QShortcut::activated, [=](int arg=1) {
        bit_alignment_scroller(arg);
    } );

    connect(bit_alignment_decrease_shortcut, &QShortcut::activated, [=](int arg=-1) {
        bit_alignment_scroller(arg);
    } );
    visualization.set_reverse_draw((bool)atoi(constant_variables->get("reverse_draw")->getData()));
    QPalette palette = get_palette();
    this->setPalette(palette);
    settings = NULL;
    byte_plot_window = NULL;
    create_status_bar();
    filesize = 0;

}

MainWindow::~MainWindow()
{
    constant_variables->save();
    global_task_manager.stop();
    delete constant_variables;
    delete ui;
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    INFO("Switching shader to vertex_%s.glsl", arg1.toLower().toStdString().c_str());
    ui->opengl_window->init_shaders((char*)((QString)("vertex_" + arg1.toLower() + ".glsl")).toStdString().c_str(), "fragment.glsl");
}

void MainWindow::on_combo_rotation_activated(const QString &arg1)
{
    if(arg1 == "0/0"){
        visualization.set_rotation_x(0);
        visualization.set_rotation_y(0);
    }
    else if(arg1 == "90/0"){
        visualization.set_rotation_x(90);
        visualization.set_rotation_y(0);
    }
    else if(arg1 == "0/90"){
        visualization.set_rotation_x(0);
        visualization.set_rotation_y(90);
    }
    else if(arg1 == "90/90"){
        visualization.set_rotation_x(90);
        visualization.set_rotation_y(90);
    }
    rotation_label->setText(QString::number(visualization.get_rotation_x()) + "," + QString::number(visualization.get_rotation_y()));
}

void MainWindow::file_open_clicked() {
    QString path = constant_variables->get("last_folder")->getData();
    QString filename = QFileDialog::getOpenFileName(this, tr("Open File"), path, tr("Files (*)"));

    if (filename.length() > 0) {
        open_file(filename);

        //Not extremely elegant, but should at least not have any memory leaks
        QDir dir = QFileInfo(filename).absoluteDir();
        QByteArray byteArray = dir.absolutePath().toUtf8();
        char* cDir = byteArray.data();
        constant_variables->set("last_folder", cDir, strlen(cDir));
    }
}

void MainWindow::open_file(QString filename) {
    file_path_label->setText("Loading: " + filename);
    ui->centralWidget->repaint();
    current_file = filename;
    visualization.set_file(filename.toStdString().c_str());
    if(byte_plot_window != NULL){
        byte_plot_window->set_data_container(&(visualization.file_controller.data));
        byte_plot_window->update_image();
        byte_plot_window->reset_image_params();
    }
    file_path_label->setText("Now analyzing: " + filename);
    filesize = visualization.get_file_size();

    //Slider initialization
    ui->windowSlider->setSliderPosition(ui->windowSlider->maximum()); //Window size is max at file load
    ui->windowSlider->setEnabled(true);
    //ui->positionSlider->setMaximum(filesize);
    ui->positionSlider->setSliderPosition(0);
    ui->positionSlider->setEnabled(true);
    //ui->positionSlider->setTickInterval(filesize/100);
    offset_handle();
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    visualization.set_min_filter(MAX(arg1.toInt(), 1));
}

void MainWindow::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();
    if (urls.size() == 1) {
        QUrl url = urls.first();
        QString filename = url.toLocalFile();
        open_file(filename);
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}

void MainWindow::settings_clicked()
{
    if(settings == NULL){
        settings = new settings_window(this);

        settings->red = atoi(constant_variables->get("palette_red")->getData());
        settings->green = atoi(constant_variables->get("palette_green")->getData());
        settings->blue = atoi(constant_variables->get("palette_blue")->getData());
        settings->orthographical_view = atoi(constant_variables->get("orthographical_view")->getData());
        settings->reverse_draw = atoi(constant_variables->get("reverse_draw")->getData());
        settings->update_all();

        connect(settings, SIGNAL(accepted()), this, SLOT(settings_accepted()));
    }
    settings->show();
}

void MainWindow::settings_accepted()
{
    INFO("Settings accepted with color(%d,%d,%d);", settings->red, settings->green, settings->blue);
    QPalette palette = get_palette();
    palette.setColor(QPalette::Window, QColor(settings->red, settings->green, settings->blue, 255));

    char buf[64] = "";

    sprintf(buf, "%d", settings->red);
    constant_variables->set("palette_red", buf, strlen(buf));

    sprintf(buf, "%d", settings->green);
    constant_variables->set("palette_green", buf, strlen(buf));

    sprintf(buf, "%d", settings->blue);
    constant_variables->set("palette_blue", buf, strlen(buf));

    this->setPalette(palette);
    this->setPalette(get_palette());

    //Orthographical checkbox
    ui->opengl_window->toggle_orthographical_view(settings->orthographical_view);
    sprintf(buf, "%d", settings->orthographical_view);
    constant_variables->set("orthographical_view", buf, strlen(buf));

    sprintf(buf, "%d", settings->reverse_draw);
    constant_variables->set("reverse_draw", buf, strlen(buf));
    visualization.set_reverse_draw((bool)settings->reverse_draw);
    ui->opengl_window->request_frame_redraw();

    //Delete window
    delete settings;
    settings = NULL;
}

void MainWindow::toggleFullScreen() {
    if(!isFullScreen()) {
        return_to_maximized = isMaximized();
    }
    if(isFullScreen() && !return_to_maximized){
        showNormal();
    } else if(isFullScreen() && return_to_maximized) {
        showMaximized();
    } else {
        showFullScreen();
    }
}

void MainWindow::on_button_set_offsets_clicked()
{
    long int start = ui->text_start_offset->text().toLong();
    long int stop = ui->text_stop_offset->text().toLong();
    visualization.set_offsets(start, stop);
}

void MainWindow::offset_handle()
{
    long int window_size = offset_from_thousandths(ui->windowSlider->value(), filesize);
    if (window_size < 3)
    {
        window_size = 3;
    }
    long int window_pos = offset_from_thousandths(ui->positionSlider->value(), filesize);
    INFO("Window_size = %ld , Window_pos = %ld, Filesize = %ld", window_size, window_pos, filesize);
    long int start_offset = window_pos;
    long int stop_offset = (window_pos + window_size) -1 ; //Minus 1 since we are 0-indexed

    if (stop_offset > visualization.get_file_size()) {
        stop_offset = visualization.get_file_size();
    }
    visualization.set_offsets(start_offset, stop_offset);
    update_labels(window_size, start_offset, stop_offset);
}

void MainWindow::update_labels(long int window_size, long int window_start, long int window_stop) {
    if (window_start == 0) {
        window_pos_label->setText("0 B - " + formatBytes(window_stop));
    }
    else {
        window_pos_label->setText(formatBytes(window_start) + " - " + formatBytes(window_stop));
    }
    window_size_label->setText(formatBytes(window_size));
}

void MainWindow::window_scroller(bool direction) {
    if (direction) {
        ui->positionSlider->setValue(ui->positionSlider->value() + ui->windowSlider->value());
    }
    else {
        ui->positionSlider->setValue(ui->positionSlider->value() - ui->windowSlider->value());
    }
    offset_handle();
}

void MainWindow::bit_alignment_scroller(int direction)
{
    ui->dropdown_bit_alignment->setCurrentIndex(ui->dropdown_bit_alignment->currentIndex() + direction);
}

void MainWindow::bit_alignment_modified()
{
    int val = atoi(ui->dropdown_bit_alignment->currentText().toStdString().c_str());
    if (val != 0)
    {
        visualization.set_bit_alignment(val);
        MainWindow::bit_alignment_label->setText(QString::number(val));
    }
}

void MainWindow::point_scaling_modified()
{
    visualization.set_point_scaling(ui->point_scaling_combobox->currentIndex());
}

void MainWindow::create_status_bar()
{
    file_path_label = new QLabel("Initializing");
    file_path_label->setAlignment(Qt::AlignLeft);
    file_path_label->setMinimumSize(file_path_label->sizeHint());

    window_pos_label = new QLabel("0 B");
    window_pos_label->setAlignment(Qt::AlignCenter);
    window_pos_label->setToolTip("Window position");

    window_size_label = new QLabel("3 B");
    window_size_label->setAlignment(Qt::AlignCenter);
    window_size_label->setToolTip("Window size");

    zoom_level_label = new QLabel("1.0");
    zoom_level_label->setAlignment(Qt::AlignCenter);
    zoom_level_label->setToolTip("Zoom level");

    point_size_label = new QLabel(QString::number(ui->point_size_slider->value()));
    point_size_label->setAlignment(Qt::AlignCenter);
    point_size_label->setToolTip("Point size");

    transparency_label = new QLabel(QString::number(ui->transparency_slider->value()));
    transparency_label->setAlignment(Qt::AlignCenter);
    transparency_label->setToolTip("Transperency");

    rotation_label = new QLabel("0,0");
    rotation_label->setAlignment(Qt::AlignCenter);
    rotation_label->setToolTip("Rotation");

    steps_label = new QLabel(QString::number(ui->steps_slider->value()));
    steps_label->setAlignment(Qt::AlignCenter);
    steps_label->setToolTip("Steps");

    bit_alignment_label = new QLabel("8"); //Very magic
    bit_alignment_label->setAlignment(Qt::AlignCenter);
    bit_alignment_label->setToolTip("Bit alignment");

    statusBar()->addPermanentWidget(file_path_label, 1);
    statusBar()->addPermanentWidget(window_size_label);
    statusBar()->addPermanentWidget(window_pos_label);
    statusBar()->addPermanentWidget(zoom_level_label);
    statusBar()->addPermanentWidget(point_size_label);
    statusBar()->addPermanentWidget(transparency_label);
    statusBar()->addPermanentWidget(rotation_label);
    statusBar()->addPermanentWidget(steps_label);
    statusBar()->addPermanentWidget(bit_alignment_label);
}

void MainWindow::copy_file_info_to_clipboard() {
    QClipboard *clipboard = QApplication::clipboard();
    //Should be a struct...?
    QString file_info =
            current_file + "|"
            + QString::number(ui->windowSlider->value()) + "|"
            + QString::number(ui->positionSlider->value()) + "|"
            + zoom_level_label->text() + "|"
            + point_size_label->text() + "|"
            + transparency_label->text() + "|"
            + rotation_label->text() + "|"
            + steps_label->text() + "|"
            + bit_alignment_label->text() + "|"
            + QString("BINVIS");
    INFO("%s", qPrintable("Copying to clipboard: " + file_info));
    clipboard->setText(file_info);
}

void MainWindow::read_file_info_from_clipboard() {
    QClipboard *clipboard = QApplication::clipboard();
    QString s = clipboard->text();
    QStringList sl = s.split("|");

    if(sl.count() == 10)
    {
        printf("NUMBER OF SUBSTRINGS: %d\nDEBUG: \n", sl.count());

        for (QString a : sl) {
            printf("\t%s\n", a.toLower().toStdString().c_str());
        }

        if(QString::compare(sl.at(9), "BINVIS", Qt::CaseSensitive) == 0) //Insane validation
        {
            //Set file -- iff file is different than current
            if (current_file != sl.at(0))
            {
                open_file(sl.at(0));
            }

            //Set window size
            ui->windowSlider->setValue(sl.at(1).toInt());

            //Set window pos
            ui->positionSlider->setValue(sl.at(2).toInt());
            offset_handle();

            //Set zoom level
            ui->opengl_window->set_position_scale_float(sl.at(3).toFloat());

            //Set point size
            ui->opengl_window->set_point_size(sl.at(4).toInt());
            ui->point_size_slider->setValue(sl.at(4).toInt());

            //Set transparency
            ui->opengl_window->set_point_alpha(sl.at(5).toInt());
            ui->transparency_slider->setValue(sl.at(5).toInt());

            //Set rotation
            QStringList rot = sl.at(6).split(",");
            visualization.set_rotation_x(rot.at(0).toInt());
            visualization.set_rotation_y(rot.at(1).toInt());

            //Set steps
            ui->opengl_window->set_step_count(sl.at(7).toInt());
            ui->steps_slider->setValue(sl.at(7).toInt());

            //Set bit alignment
            ui->dropdown_bit_alignment->currentText().toStdString().c_str();
            ui->dropdown_bit_alignment->setCurrentText(sl.at(8));
        }
    }
}

void MainWindow::byte_plot_clicked()
{
    if(byte_plot_window == NULL){
        byte_plot_window = new byte_plot(this);
        byte_plot_window->setWindowFlags(Qt::Window | Qt::WindowSystemMenuHint);
        byte_plot_window->set_data_container(&(visualization.file_controller.data));
        byte_plot_window->update_image();
        connect(byte_plot_window, &QObject::destroyed, [=] () {
            byte_plot_window = NULL;
        });
    }
    if (byte_plot_window->isHidden()) {
        byte_plot_window->show();
        return;
    }
    byte_plot_window->saveGeometry();
    byte_plot_window->hide();

}

QPalette MainWindow::get_palette() {
    QPalette palette = this->palette();
    QColor clr = QColor(
                atoi(constant_variables->get("palette_red")->getData()),
                atoi(constant_variables->get("palette_green")->getData()),
                atoi(constant_variables->get("palette_blue")->getData()),
                255);
    palette.setColor(QPalette::Window, clr);

    //Ugly
    char clrbfr[64] = "";
    sprintf(clrbfr, "(%d, %d, %d);", clr.red(), clr.green(), clr.blue());
    QString clsr = QString::fromUtf8(clrbfr);

    //To avoid black text on black background
    if (palette.color(QPalette::Window).lightness() < 90) {
        ui->tabWidget->setStyleSheet(QString::fromUtf8("background-color: rgb") + clsr + QString::fromUtf8("\n color:  rgb(255,255,255);"));
        ui->tabWidget->tabBar()->setStyleSheet(QString::fromUtf8("background-color: rgb") + clsr + QString::fromUtf8("\n color:  rgb(255,255,255);"));
        palette.setColor(QPalette::WindowText, Qt::white);
    } else {
        ui->tabWidget->setStyleSheet(QString::fromUtf8("background-color: rgb") + clsr + QString::fromUtf8("\n color:  rgb(0,0,0);"));
        ui->tabWidget->tabBar()->setStyleSheet(QString::fromUtf8("background-color: rgb") + clsr + QString::fromUtf8("\n color:  rgb(0,0,0);"));
        palette.setColor(QPalette::WindowText, Qt::black);
    }
    return palette;
}

void MainWindow::hide_menu_slot() {
    if (this->ui->widget->isHidden())
    {
        this->ui->widget->show();
    }
    else
    {
        this->ui->widget->hide();
    }
}
