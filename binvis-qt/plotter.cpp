/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <QTime>

#include "plotter.h"
#include "displaydata.h"
#include "error_messages.h"
#include "alignment_tables.h"
#include "alignment_tables.cpp"
#include "math.h"
#include "global_task_manager.h"
#include "task_byte_plotter.h"
#include "task_data_plotter.h"
#include "ratelimited_task_manager.h"
#include <limits.h>
#include <stdint.h>

#ifdef USE_MMAP
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#endif
#define MULTITHREADED_PLOTTER
#define add_point(x,y,z) display_data->vertices.points[z*256*256 + y*256 + x].count+=1.0f
#define remove_point(x,y,z) display_data->vertices.points[z*256*256 + y*256 + x].count-=1.0f
#define add_addvalue_to_point(x,y,z) display_data->vertices.points[z*256*256 + y*256 + x].count+=add_value
#define read_size 8388608
#define nothing_read(a) (a <= 0)
#define more_data_available_and_place_in(a) (bytes_to_add > 0 && (a = get_next_value_from_filedatabuffer(&data)) >= 0)

void add_point_to_displaydata(struct displaydata* display_data, float add_value, int x, int y, int z){
#ifdef _WIN32
	EnterCriticalSection(&(display_data->vertices.point_mutex[x]));
#else
	display_data->vertices.point_mutex[x].lock();
#endif

	add_addvalue_to_point(x,y,z);

#ifdef _WIN32
	LeaveCriticalSection(&(display_data->vertices.point_mutex[x]));
#else
	display_data->vertices.point_mutex[x].unlock();
#endif
}

void add_point_to_displaydata_lockfree(struct displaydata* display_data, float add_value, int x, int y, int z){
    //Index will be used multiple times, store it
    int ind = z*256*256 + y*256 + x;
    //Pointer to count to be changed
    long long* ptr = (long long*)&(display_data->vertices.points[ind].count);
    /* (We go for most-common implementation here; long long can be 32-bit etc etc)
     * __sync_bool_compare_and_swap requires a 64-bit structure to swap, float is 32
     * union of long long (to swap) and 2 floats
     */
    union{
        long long l;
        struct{
            float f[2];
        };
    }swapdata, currentdata;

    do{
        /*
         * swapdata.l is set to a pointer to the 64 bits of 'data' that starts at .count
         * It is at the time of writing .count and .x, but not to be relied on
         */
        swapdata.l = *(long long*)&display_data->vertices.points[ind].count;
        currentdata.l = swapdata.l;
        /*
         * Add value to the point in local copy of the data
         */
        swapdata.f[0] = swapdata.f[0]+add_value;
    }while(!__sync_bool_compare_and_swap(ptr,
                                         currentdata.l,
                                         swapdata.l
                                         ));
}

struct {
	struct alignment_round* alignment;
	long long unsigned int highest_value;
}alignment_tables[] = {
{NULL,   0},//Unused
{alignment_ptr_1bit,   1},//1-bit
{alignment_ptr_2bit,   3},//2-bit
{alignment_ptr_3bit, 7},//3-bit
{alignment_ptr_4bit, 15},//4-bit
{alignment_ptr_5bit,   31},   //5-bit
{alignment_ptr_6bit,   63},   //6-bit
{alignment_ptr_7bit,   127},  //7-bit
{alignment_ptr_8bit, 255},//8-bit
{alignment_ptr_16bit,   65535},//16-bit
{alignment_ptr_24bit,   16777215},//24-bit
{alignment_ptr_32bit,   4294967295},//32-bit
{alignment_ptr_48bit,   281474976710655},//48-bit
{NULL,   18446744073709551615UL}//64-bit
};

/*
 * Parses a file and maps each set of sequencial bytes {x,y,z} to the map in display_data
 */
long int plot_file(char* filename, displaydata* display_data){
	return process_partfile_to_displaydata(filename, display_data, 0, LONG_MAX-1, 1.0f, INITIAL);
}

void add_buffer_to_displaydata(char* buffer, unsigned int count, displaydata* display_data){
	union{
		int val;
		struct{ unsigned char w, x, y, z; };
	}chars = {0};
	chars.val = buffer[0] << 8 | buffer[1];
	for(unsigned int n = 2; n < count; ++n){
		chars.val = (chars.val << 8) | buffer[n];
		add_point(chars.x, chars.y, chars.z);
	}
}

void remove_buffer_from_displaydata(char* buffer, unsigned int count, displaydata* display_data){
	union{
		int val;
		struct{ unsigned char w, x, y, z; };
	}chars = {0};
	chars.val = (buffer[0] << 8 | buffer[1]);

	for(unsigned int n = 2; n < count; ++n){
		chars.val = (chars.val << 8) | buffer[n];
		remove_point(chars.x, chars.y, chars.z);
	}
}

long int process_partfile_to_displaydata(const char* filename, displaydata* display_data, long int first_byte, long int last_byte, float add_value, add_location_t location){
	INFO("Using optimized 8-bit method");
	FILE* file = fopen(filename, "rb");
	if(!file){
		ERROR("Could not open file.");
		return -1;
	}

	//Seek to initial position
	fseek(file, first_byte-((first_byte >=2 && location == END)?2 : 0), SEEK_SET);
	long int bytes_to_add = last_byte-first_byte+1-((location == INITIAL)?2:0);

	int bytes_in_buffer = 0;
	unsigned char buffer[read_size];

	union{
		int val;
		struct{ unsigned char w, x, y, z; };
	}chars = {0};
	int start_position = 2; //2 if this is first read round, 0 otherwise
	while((bytes_in_buffer = fread(buffer, 1, read_size, file)) > 0){
		if(start_position > 0){
			chars.y = buffer[0];
			chars.x = buffer[1];
		}
		for(int n = start_position; n < bytes_in_buffer && bytes_to_add > 0; ++n){
			/*w=x, x=y, y=z, z=buffer[n]*/
			chars.val = (chars.val | buffer[n]) << 8;
			add_addvalue_to_point(chars.x, chars.y, chars.z);
			--bytes_to_add;
		}
		start_position = 0;
	}
	long int filepos = ftell(file);
	fclose(file);
	return filepos;
}

void add_partfile_to_displaydata(char* filename, displaydata* display_data, long int first_byte, long int last_byte, add_location_t location, int alignment, int scaling){
	process_partfile_to_displaydata_alignment(filename, display_data, first_byte, last_byte, 1.0f, location, alignment, scaling);
}

void remove_partfile_from_displaydata(char* filename, displaydata* display_data, long int first_byte, long int last_byte, add_location_t location, int alignment, int scaling){
	process_partfile_to_displaydata_alignment(filename, display_data, first_byte, last_byte, -1.0f, location, alignment, scaling);
}

#ifdef USE_MMAP
int
#else
FILE*
#endif
fopen_at_location(const char* filename, long int first_byte, add_location_t location){
#ifdef USE_MMAP
	int file = open64(filename, O_RDONLY);
#else
	FILE* file = fopen(filename, "rb");
#endif
	if(!file){
		ERROR("Could not open file.");
		return file;
	}
#ifdef USE_MMAP
	lseek(file, first_byte-((first_byte >=2 && location == END)?2 : 0), SEEK_SET);
#else
	//Seek to initial position
	fseek(file, first_byte-((first_byte >=2 && location == END)?2 : 0), SEEK_SET);
#endif
	return file;
}

struct alignment_round* get_alignment_table(int alignment){
	if(alignment <= 8)
		return alignment_tables[alignment].alignment;
	else if (alignment == 16)
		return alignment_tables[9].alignment;
	else if (alignment == 24)
		return alignment_tables[10].alignment;
	else if (alignment == 32)
		return alignment_tables[11].alignment;
	else if (alignment == 48)
		return alignment_tables[12].alignment;
	return NULL;
}

long long get_alignment_table_max_value(int alignment)
{
	if(alignment <= 8)
		return alignment_tables[alignment].highest_value;
	else if (alignment == 16)
		return alignment_tables[9].highest_value;
	else if (alignment == 24)
		return alignment_tables[10].highest_value;
	else if (alignment == 32)
		return alignment_tables[11].highest_value;
	else if (alignment == 48)
		return alignment_tables[12].highest_value;
	return 0;
}

void add_datapoint_to_data_container(unsigned char val, struct data_container* data){
	if(data == NULL)
		return;
	if(data->data_pos >= data->size)
		return;
	data->data_ptr[data->data_pos++] = static_cast<unsigned char>(val);
}

long int process_partfile_to_displaydata_alignment(const char* filename, displaydata* display_data, long int first_byte, long int last_byte, float add_value, add_location_t location, int alignment, int scaling, data_container *data_cont){
	INFO("Parsing file with alignment=%d", alignment);
	//If alignment is 8, use function optimized for 8
	/*
	if(alignment == 8)
	return process_partfile_to_displaydata(filename, display_data, first_byte, last_byte, add_value, location);
	*/

#ifdef USE_MMAP
	int
		#else
	FILE*
		#endif
			file = fopen_at_location(filename, first_byte, location);
	if(!file){
		ERROR("Could not open file: %s", filename);
		return -1;
	}
	/*
     * We have a buffer ABCDE
     * If this is the initial addition of the buffer we only want to add ABC, BCD, CDE, and not DE? or E??
     * The next line takes care of that
     */
	long int bytes_to_add = last_byte-first_byte+1-((location == INITIAL)?2:0);
	//long long points_added = 0;
	union{
		int val;
		struct{ unsigned char w, x, y, z; };
	}chars = {0};
	QTime time;
	time.start();
	struct filedatabuffer data = create_filedatabuffer(file, alignment, scaling, data_cont);
	long long normalized_value;
#ifndef MULTITHREADED_PLOTTER
	while(bytes_to_add > 0 && (normalized_value = get_next_scaled_value_from_filedatabuffer(&data)) >= 0){
		chars.val = (chars.val | normalized_value) << 8;
		//add_addvalue_to_point(chars.x, chars.y, chars.z);
		add_point_to_displaydata(display_data, add_value, chars.x, chars.y, chars.z);
		//add_datapoint_to_data_container(chars.x, data_cont);
		bytes_to_add--;
		//points_added++;
	}
#else
	//32 threads (more than needed), as many of them might be waiting for mutexes a lot of the time
	Ratelimited_task_manager local_manager(32, 32);
	chars.x = get_next_scaled_value_from_filedatabuffer(&data);
	chars.y = get_next_scaled_value_from_filedatabuffer(&data);
	while(bytes_to_add > 0 && (normalized_value = get_next_scaled_value_from_filedatabuffer(&data)) >= 0){
		if(bytes_to_add < read_size)
			data.bytes_in_buffer = bytes_to_add;
		data.position_in_file_buffer--;
		Task_data_plotter* task = new Task_data_plotter;
		task->set_properties(&data, chars.x, chars.y, display_data, add_value);
		task->set_delete_on_complete(true);
		local_manager.add_task(task, true);
		bytes_to_add -= data.bytes_in_buffer;
		data.position_in_file_buffer = data.bytes_in_buffer-2;
		chars.x = get_next_scaled_value_from_filedatabuffer(&data);
		chars.y = get_next_scaled_value_from_filedatabuffer(&data);
	}
	if(location == INITIAL)
		INFO("File read in %d ms.", time.elapsed());
	//Wait for all tasks to complete.
	local_manager.stop();
#endif
	if(location == INITIAL)
		INFO("Plotter finished in %d ms.", time.elapsed());

#ifndef USE_MMAP
	if(location == INITIAL)
		fseek(data.filep, 0, SEEK_END);
	long int filepos = ftell(data.filep);
	fclose(file);
#else
	long int filepos = data.position_in_file_buffer;
	close(data.filep);
#endif
	destroy_filedatabuffer(data);

	return filepos;
	//return filepos;
}

struct alignment_round* get_alignment(struct filedatabuffer* data){
	return &(data->alignments[data->position_in_alignment_table]);
}

unsigned char get_current_buffer_byte(struct filedatabuffer* data){
	return data->buffer[data->position_in_file_buffer];
}

size_t read_next_buffer_to_filedatabuffer(struct filedatabuffer* data){
	if(data->filep == NULL)
		return -1;
#ifndef USE_MMAP
	long file_pos = ftell(data->filep);
	if(data->filep != NULL)
		data->bytes_in_buffer = fread(data->buffer, 1, read_size, data->filep);

	if(data->bytes_in_buffer > 0){
		Task_byte_plotter* task = new Task_byte_plotter();
		task->set_delete_on_complete(true);
		task->set_properties(data->byte_plot.data_cont, data, file_pos);
		data->byte_plot.manager->add_task(task);
	}
	//ERROR("Invalid file pointer. If you are running tests diregard this message.");
	return data->bytes_in_buffer;
#endif
	return -1;
}

void move_to_next_byte_in_filedatabuffer(struct filedatabuffer* data){
	data->position_in_file_buffer++;
#ifdef USE_MMAP
	return;
#endif
	if(data->position_in_file_buffer < read_size && data->position_in_file_buffer < data->bytes_in_buffer)
		return;
	int read_return = read_next_buffer_to_filedatabuffer(data);
	if(read_return >= 0)
		data->position_in_file_buffer = 0;
	/*
	if(read_return <= 0)
		INFO("Reached end of file");
	*/
}

void move_to_next_alignment_table_in_filedatabuffer(struct filedatabuffer* data){
	if(get_alignment(data)->restart_alignment)
		data->position_in_alignment_table = 0;
	else
		data->position_in_alignment_table++;
}

void add_next_bitset_to_value(struct filedatabuffer* data, long long* value){
	long long shift_mask = get_alignment(data)->shift_mask;
	/*unsigned char current_buffer_byte = get_current_buffer_byte(data);
    int read_mask = get_alignment(data)->read_mask;

    *value = (current_buffer_byte & read_mask);
    if(shift_mask > 0)
	*value = *value << shift_mask;
    else
	*value = *value >> -shift_mask;*/
	//Shift in correct direction. Shifting by negative number is always 0 on some platforms, so always shift positively.
	if(shift_mask > 0)
		*value |= ((long long)get_current_buffer_byte(data) & (long long)get_alignment(data)->read_mask) << (long long)shift_mask;
	else
		*value |= ((long long)get_current_buffer_byte(data) & (long long)get_alignment(data)->read_mask) >> (long long)-shift_mask;
}

bool no_more_data_in_filedatabuffer(struct filedatabuffer* data){
	if(data->filep == NULL && data->position_in_file_buffer >= data->bytes_in_buffer)
		return true;

#ifdef USE_MMAP
	return data->position_in_file_buffer >= data->bytes_in_buffer;
#else
	return data->position_in_file_buffer >= data->bytes_in_buffer && data->filep != NULL && feof(data->filep);
#endif
}
#ifdef USE_MMAP
struct filedatabuffer create_filedatabuffer(int file, int alignment)
#else
struct filedatabuffer create_filedatabuffer(FILE* file, int alignment, int scaling, struct data_container* data_cont)
#endif
{
	struct filedatabuffer data;
	data.position_in_alignment_table = data.position_in_file_buffer = 0;
	data.filep = file;
	data.alignments = get_alignment_table(alignment);
	data.alignment_max_value = get_alignment_table_max_value(alignment);
	data.alignment_scaling = scaling;
#ifdef USE_MMAP
	struct stat sb;
	fstat(data.filep, &sb);
	long long file_size = sb.st_size;
	long long current_position = lseek(data.filep, 0, SEEK_CUR);
	data.buffer = (unsigned char*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, data.filep, 0);
	data.position_in_file_buffer = current_position;
	data.bytes_in_buffer = file_size;
#else
	data.buffer = (unsigned char*)malloc(read_size*sizeof(unsigned char));
	data.bytes_in_buffer = 0;
#endif
	data.byte_plot.current_task = data.byte_plot.next_task = NULL;
	data.byte_plot.manager = &global_task_manager;
	data.byte_plot.data_cont = data_cont;

	move_to_next_byte_in_filedatabuffer(&data);
	return data;
}

void destroy_filedatabuffer(struct filedatabuffer data){
#ifdef USE_MMAP
	munmap(data.buffer, data.bytes_in_buffer);
#else
	free(data.buffer);
#endif
}

long long get_next_value_from_filedatabuffer(struct filedatabuffer* data){
	if(no_more_data_in_filedatabuffer(data))
		return -1;
	long long value = 0;
	bool value_finished = false;
	do{
		add_next_bitset_to_value(data, &value);
		if(get_alignment(data)->next_byte_after)
			move_to_next_byte_in_filedatabuffer(data);
		value_finished = get_alignment(data)->value_finished;
		move_to_next_alignment_table_in_filedatabuffer(data);
	}while(!value_finished && !no_more_data_in_filedatabuffer(data));
	return value;
}




long process_partfile_to_displaydata_settings(plotter_settings settings)
{
	return process_partfile_to_displaydata_alignment(settings.filename, settings.display_data, settings.first_byte, settings.last_byte, settings.add_value, settings.location, settings.alignment, settings.scaling);
}


long long get_next_scaled_value_from_filedatabuffer(struct filedatabuffer* data)
{
	long long value = get_next_value_from_filedatabuffer(data);
	if(value < 0)
		return value;
	if(data->alignment_scaling == SCALING_LINEAR)
		return ((long long)255*value)/data->alignment_max_value;
	else
		return static_cast<long long>(log10(static_cast<double>(value)) * 255.0 / log10(static_cast<double>(data->alignment_max_value)));
}
