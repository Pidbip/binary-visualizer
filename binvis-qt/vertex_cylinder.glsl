#version 130
/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

in vec3 position;
in float count;
uniform float alpha_value = 0.5;
uniform mat4 rotation;
uniform int min_count = 1;
uniform float steps;
uniform float pos_scale;
out vec4 vcolor;

void main() { 
	float n = count/(2*min_count);

	float angle = position.x*360.0;
	float distance = position.y;
	float height = position.z;
	
	float x = distance*cos(angle);
	float z = distance*sin(angle);
	float y = height;
	
        gl_Position = rotation*vec4(x, y, z, 1.0);
        gl_Position.xy = gl_Position.xy*pos_scale;
        if(count >= min_count)
                vcolor = vec4(0.0+n/steps, 1.0-n/steps, steps/200, alpha_value);
        else
                vcolor = vec4(0.0, 0.0, 0.0, 0.0);
}
